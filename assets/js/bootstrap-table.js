// import bootstrap-table
// see https://bootstrap-table.com/
import 'bootstrap-table';

// import extension and dependencies
import 'bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control'

$('#index_table').bootstrapTable({
    onClickRow: function (row, {}, {}) {
        var URL = "/part/"+row.id;
		window.location.href=URL;
    }
})
