import { Controller } from '@hotwired/stimulus';

/*
 * This stimulus controller is used to execute some functions on a newPart form
 *
 * It is executed to automatically generate the next number (after some field changes)
 * or to enable the number field when an admin click on the dedicated button
 */
export default class extends Controller {
    static targets = [ "number" ]

    async nextNumber() {
        this.numberTarget.value = 'loading...';

        var idFamily = $('#part_family').val();
        var idType = $('#part_type').val();
        const params = new URLSearchParams({
            'idFamily': idFamily,
            'idType': idType
        });

        const response = await fetch(`nextNumber/ajax?${params}`);
        this.numberTarget.value = await response.text();
    }

    // Reactivating field "number" (for specific case only) 
    activateNumber() {
        this.numberTarget.removeAttribute('readonly');
    }
}
