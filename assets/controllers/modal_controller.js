import { Controller } from '@hotwired/stimulus';
import { Modal } from 'bootstrap';

/*
 * This stimulus controller is used to show and fill a modal
 *
 * It is meant to be generic and reusable.
 * Parameters such as url for dynamic content and Title are passed as arguments
 */
export default class extends Controller {
    static targets = [ 'modal', 'modalBody' ]
    static values = {
        url: String,
    }

    async loadModal() {
        const modal = new Modal(this.modalTarget);
        modal.show();
                
        this.modalBodyTarget.innerHTML = await $.ajax(this.urlValue);
        // Or, without jQuery
        //const response = await fetch(this.urlValue);
    }

}
