/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// Bootstrap css and icons are loaded from app.scss
import './styles/app.scss';

// Import bootstrap javascripts
import 'bootstrap'; // add functions to jQuery
import bsCustomFileInput from 'bs-custom-file-input';

// start the Stimulus application
import './bootstrap';

// Initialize bootstrap file input script
bsCustomFileInput.init();

// Import images from the img folder
// See https://io.serendipityhq.com/experience/managing-static-images-webpack-encore/
const imagesContext = require.context('./images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

// create global $ and jQuery variables
// Necessary to access jQuery outside of Webpack
// See: https://symfony.com/doc/current/frontend/encore/legacy-applications.html#accessing-jquery-from-outside-of-webpack-javascript-files
global.$ = global.jQuery = $;
