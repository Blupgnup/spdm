<?php

namespace App\Entity;

use App\Repository\PartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PartRepository::class)]
class Part
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Project::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $family;

    #[ORM\ManyToOne(targetEntity: Type::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $type;

    #[ORM\Column(type: 'string', length: 5)]
    #[Assert\Type(
        type: 'digit',
        message: 'The value {{ value }} is not a valid input. (5 digits, positive)',
    )]
    #[Assert\Length(
        min: 5,
        max: 5,
        exactMessage: 'The part number must be a {{ limit }} digit number',
    )]
    private $number;

    #[ORM\Column(type: 'string', length: 50)]
    #[Assert\NotBlank]
    private $designation;

    #[ORM\Column(type: 'string', length: 2, nullable: true)]
    private $majorRev;

    #[ORM\Column(type: 'string', length: 2, nullable: true)]
    #[Assert\Positive]
    #[Assert\Type(
        type: 'digit',
        message: 'The value {{ value }} is not a valid input. (Two digits, positive)',
    )]
    private $minorRev;

    #[ORM\Column(type: 'boolean')]
    private $isCAD;

    #[ORM\Column(type: 'boolean')]
    private $isObsolete;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private $linkedRef;

    #[ORM\ManyToMany(targetEntity: Project::class)]
    private $projects;

    #[ORM\ManyToMany(targetEntity: Supplier::class)]
    private $suppliers;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private $supplierRef;

    #[ORM\ManyToMany(targetEntity: Material::class)]
    private $materials;

    #[ORM\Column(type: 'text', nullable: true)]
    private $commentary;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $pictureFilename;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $linkedFileFilename;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $createdAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $updatedAt;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $lastUser;

    #[ORM\OneToMany(mappedBy: 'Part', targetEntity: Document::class, orphanRemoval: true)]
    private $documents;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->suppliers = new ArrayCollection();
        $this->materials = new ArrayCollection();
        $this->documents = new ArrayCollection();

        $this->majorRev = "A";
        $this->minorRev = "01";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFamily(): ?Project
    {
        return $this->family;
    }

    public function setFamily(?Project $family): self
    {
        $this->family = $family;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getMajorRev(): ?string
    {
        return $this->majorRev;
    }

    public function setMajorRev(?string $majorRev): self
    {
        $this->majorRev = $majorRev;

        return $this;
    }

    public function getMinorRev(): ?string
    {
        return $this->minorRev;
    }

    public function setMinorRev(?string $minorRev): self
    {
        $this->minorRev = $minorRev;

        return $this;
    }

    public function getIsCAD(): ?bool
    {
        return $this->isCAD;
    }

    public function setIsCAD(bool $isCAD): self
    {
        $this->isCAD = $isCAD;

        return $this;
    }

    public function getIsObsolete(): ?bool
    {
        return $this->isObsolete;
    }

    public function setIsObsolete(bool $isObsolete): self
    {
        $this->isObsolete = $isObsolete;

        return $this;
    }

    public function getLinkedRef(): ?string
    {
        return $this->linkedRef;
    }

    public function setLinkedRef(?string $linkedRef): self
    {
        $this->linkedRef = $linkedRef;

        return $this;
    }

    /**
     * @return Collection<int, Project>
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        $this->projects->removeElement($project);

        return $this;
    }

    /**
     * @return Collection<int, Supplier>
     */
    public function getSuppliers(): Collection
    {
        return $this->suppliers;
    }

    public function addSupplier(Supplier $supplier): self
    {
        if (!$this->suppliers->contains($supplier)) {
            $this->suppliers[] = $supplier;
        }

        return $this;
    }

    public function removeSupplier(Supplier $supplier): self
    {
        $this->suppliers->removeElement($supplier);

        return $this;
    }

    public function getSupplierRef(): ?string
    {
        return $this->supplierRef;
    }

    public function setSupplierRef(?string $supplierRef): self
    {
        $this->supplierRef = $supplierRef;

        return $this;
    }

    /**
     * @return Collection<int, Material>
     */
    public function getMaterials(): Collection
    {
        return $this->materials;
    }

    public function addMaterial(Material $material): self
    {
        if (!$this->materials->contains($material)) {
            $this->materials[] = $material;
        }

        return $this;
    }

    public function removeMaterial(Material $material): self
    {
        $this->materials->removeElement($material);

        return $this;
    }

    public function getCommentary(): ?string
    {
        return $this->commentary;
    }

    public function setCommentary(?string $commentary): self
    {
        $this->commentary = $commentary;

        return $this;
    }

    public function getPictureFilename(): ?string
    {
        return $this->pictureFilename;
    }

    public function setPictureFilename(?string $pictureFilename): self
    {
        $this->pictureFilename = $pictureFilename;

        return $this;
    }

    public function getLinkedFileFilename(): ?string
    {
        return $this->linkedFileFilename;
    }

    public function setLinkedFileFilename(?string $linkedFileFilename): self
    {
        $this->linkedFileFilename = $linkedFileFilename;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLastUser(): ?string
    {
        return $this->lastUser;
    }

    public function setLastUser(?string $lastUser): self
    {
        $this->lastUser = $lastUser;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->document->contains($document)) {
            $this->documents[] = $document;
            $document->setPart($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getPart() === $this) {
                $document->setPart(null);
            }
        }

        return $this;
    }
}
