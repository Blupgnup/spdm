<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TypeRepository::class)]
class Type
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 5)]
    #[Assert\NotBlank]
    #[Assert\Type(
        type: 'alpha',
        message: 'The value {{ value }} is not a valid alphabetic code ([A-Za-z]).',
    )]
    #[Assert\Length(
        min: 2,
        max: 5,
        minMessage: 'The type code must be at least {{ limit }} characters long',
        maxMessage: 'The type code cannot be longer than {{ limit }} characters',
    )]
    private $code;

    #[ORM\Column(type: 'string', length: 100)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 3,
        max: 100,
        minMessage: 'The type description must be at least {{ limit }} characters long',
        maxMessage: 'The type description cannot be longer than {{ limit }} characters',
    )]
    private $signification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getSignification(): ?string
    {
        return $this->signification;
    }

    public function setSignification(string $signification): self
    {
        $this->signification = $signification;

        return $this;
    }
}
