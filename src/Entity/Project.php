<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
class Project
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 30)]
    #[Assert\NotBlank]
    private $name;

    #[ORM\Column(type: 'string', length: 6)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 2,
        max: 6,
        minMessage: 'The project code must be at least {{ limit }} characters long',
        maxMessage: 'The project code cannot be longer than {{ limit }} characters',
    )]
    private $code;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(
        min: 6,
        max: 100,
        minMessage: 'The project description must be at least {{ limit }} characters long',
        maxMessage: 'The project description cannot be longer than {{ limit }} characters',
    )]
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getChoiceLabel(): ?string {
        return $this->code . ' - ' . $this->name;
    }
}
