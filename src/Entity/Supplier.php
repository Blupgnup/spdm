<?php

namespace App\Entity;

use App\Repository\SupplierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SupplierRepository::class)]
class Supplier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 3,
        max: 50,
        minMessage: 'The supplier\'s name must be at least {{ limit }} characters long',
        maxMessage: 'The supplier\'s name cannot be longer than {{ limit }} characters',
    )]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 4,
        max: 255,
        minMessage: 'The field description must be at least {{ limit }} characters long',
        maxMessage: 'The field description cannot be longer than {{ limit }} characters',
    )]
    private $field;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(
        min: 5,
        max: 255,
        minMessage: 'The address must be at least {{ limit }} characters long',
        maxMessage: 'The address cannot be longer than {{ limit }} characters',
    )]
    private $address;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Assert\Length(
        min: 3,
        max: 50,
        minMessage: 'The contact\'s name must be at least {{ limit }} characters long',
        maxMessage: 'The contact\'s name cannot be longer than {{ limit }} characters',
    )]
    private $contactName;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Assert\Length(
        min: 3,
        max: 50,
        minMessage: 'The contact\'s function must be at least {{ limit }} characters long',
        maxMessage: 'The contact\'s function cannot be longer than {{ limit }} characters',
    )]
    private $contactFunction;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Email(
        message: 'The email {{ value }} is not a valid email.',
    )]
    private $email;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Assert\Length(
        min: 8,
        max: 20,
        minMessage: 'The phone number must be at least {{ limit }} characters long',
        maxMessage: 'The phone number cannot be longer than {{ limit }} characters',
    )]
    private $phoneStd;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Assert\Length(
        min: 8,
        max: 20,
        minMessage: 'The phone number must be at least {{ limit }} characters long',
        maxMessage: 'The phone number cannot be longer than {{ limit }} characters',
    )]
    private $phoneDir;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comments;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(?string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactFunction(): ?string
    {
        return $this->contactFunction;
    }

    public function setContactFunction(?string $contactFunction): self
    {
        $this->contactFunction = $contactFunction;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneStd(): ?string
    {
        return $this->phoneStd;
    }

    public function setPhoneStd(?string $phoneStd): self
    {
        $this->phoneStd = $phoneStd;

        return $this;
    }

    public function getPhoneDir(): ?string
    {
        return $this->phoneDir;
    }

    public function setPhoneDir(?string $phoneDir): self
    {
        $this->phoneDir = $phoneDir;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }
}
