<?php

namespace App\EntityListener;

use App\Entity\Part;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PartEntityListener
{    
    public function prePersist(Part $part, LifecycleEventArgs $event)
    {
        $part->setCreatedAt(new \DateTimeImmutable());
        $part->setIsObsolete(false);
    }

    public function preUpdate(Part $part, LifecycleEventArgs $event)
    {
        $part->setUpdatedAt(new \DateTimeImmutable());
    }
}
