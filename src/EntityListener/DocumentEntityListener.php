<?php

namespace App\EntityListener;

use App\Entity\Document;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DocumentEntityListener
{
    private $params;
    private $flash;

    public function __construct(ParameterBagInterface $params, FlashBagInterface $flash)
    {
        $this->params = $params;
        $this->flash = $flash;
    }

    public function prePersist(Document $document)
    {          	
        // We cannot use the safer gessExtension method as this will not work for 
        // step files, dxf or other specific files. Therefore, we have to trust user input...
        // We could had a security check on MimeType to be more secure
        $ext = $document->getFile()->getClientOriginalExtension();
    	
    	// We create the title attribute based on the client original name
        // The condition is used to allow a controller to force the title by setting it before persisting the file        
        if (null === $document->getTitle()) {
    		$document->setTitle(basename($document->getFile()->getClientOriginalName(), ".". $ext));
    	}
    	// We generate the destination folder name if none defined
    	if (null === $document->getDirectory()) {
    		$document->setDirectory('uploads/files');
    	}
        // We also generate our own filename
        $document->setFilename($document->getTitle().".".$ext);

        // Move the file to the directory where documents are stored
        try {
            $document->getFile()->move($this->params->get('root_dir').$document->getDirectory(), $document->getFilename());
        } catch (FileException $e) {
            // unable to upload the file
            $this->flash->add('danger', "Unable to upload the file.");
        }
    }

    public function preRemove(Document $document)
    {
    	// On sauvegarde temporairement le nom du fichier car il dépend du titre
    	$document->setTempFilename($document->getFilename());
    }

    public function postRemove(Document $document)
    {
    	// En PostRemove, on n'a pas accès à  l'id, on utilise notre nom sauvegardé
    	if (file_exists($this->params->get('root_dir').$document->getDirectory().'/'.$document->getTempFilename())) {
    		// On supprime le fichier
    		try {
                unlink($this->params->get('root_dir').$document->getDirectory().'/'.$document->getTempFilename());
                //$document->setTempFilename(null);
            } catch (FileException $e) {
                // unable to upload the file
                $this->flash->add('danger', "Unable to remove the file.");
            }   
    	}
        else {
            $this->flash->add('danger', "The associated file does not exist.");
        }
    }
}
