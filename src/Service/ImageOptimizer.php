<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;

class ImageOptimizer
{
    private const MAX_WIDTH = 300;
    private const MAX_HEIGHT = 200;

    private $imagine;

    public function __construct()
    {
        $this->imagine = new Imagine();
    }

    public function resize(string $filename): void
    {
        list($iwidth, $iheight) = getimagesize($filename);
        $ratio = $iwidth / $iheight;
        $width = self::MAX_WIDTH;
        $height = self::MAX_HEIGHT;
        if ($width / $height > $ratio) {
            $width = $height * $ratio;
        } else {
            $height = $width / $ratio;
        }

        $image = $this->imagine->open($filename);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        if ($ext !='gif') {
            $image->resize(new Box($width, $height))->save($filename);
        }
        else {
            // We can process animated gif resize if they are proper multi-layers files
            if (count($image->layers()) > 1 ) {
                $image->layers()->coalesce();
                foreach ($image->layers() as $frame) {
                    $frame->resize(new Box($width, $height));
                }
                $image->save($filename, array('animated' => true));
            }
            else {
                //dump("The animated file is mono-layer and cannot be processed");
            }
            
        }


        
    }
}
