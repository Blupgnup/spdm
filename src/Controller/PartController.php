<?php

namespace App\Controller;

use App\Entity\Part;
use App\Entity\Document;
use App\Form\PartType;
use App\Form\PartEditType;
use App\Repository\PartRepository;
use App\Repository\DocumentRepository;
use App\Service\ImageOptimizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

#[Route('/part')]
class PartController extends AbstractController
{
    public function __construct(PartRepository $repository, ImageOptimizer $imageOptimizer, DocumentRepository $documentRepository)
    {
        $this->repository = $repository;
        $this->documentRepository = $documentRepository;
        $this->imageOptimizer = $imageOptimizer;
    }

    #[Route('/', name: 'app_part_index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('part/index.html.twig', [
            'parts' => $this->repository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_part_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $em): Response
    {
        $part = new Part();
        $form = $this->createForm(PartType::class, $part);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $part->setLastUser($this->getUser()->getUserIdentifier());
            // We need to validate the unicity of the newly create reference
            if (!$this->checkUnicity($part->getFamily(), $part->getType(), $part->getNumber(), [])) {
                // Storing uploaded picture
                if ($picture = $form['picture']->getData()) {
                    $this->setPicture($part, $picture);
                }

                // Files processing
                $primaryFile = $form->get('primaryFile')->getData();
            	$secondaryFile = $form->get('secondaryFile')->getData();
            	$document = $form->get('document')->getData();
            	if ($this->processFiles($part, $primaryFile, $secondaryFile, $document, $em)) {
                    $em->flush();
                    $this->addFlash('success', "The part <strong>". $part->getDesignation() ."</strong> has been successfully created.");
                    return $this->redirectToRoute('app_part_show', array('id' => $part->getId()), Response::HTTP_SEE_OTHER);
                }
            }
            else {
                $this->addFlash('danger', "The specified reference: <strong>" . $part->getFamily()->getCode() . $part->getType()->getCode() . $part->getNumber() . "</strong> - already exist.");
            }
        }

        return $this->renderForm('part/new.html.twig', [
            'part' => $part,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_part_show', methods: ['GET'])]
    public function show($id): Response
    {
        $part = $this->repository->getPartWithDetails($id);
        // We retrieve the last associated document by type
        $primaryFile = $this->documentRepository->getLastDocumentByPartAndType($part, 'primaryFile');
        $secondaryFile = $this->documentRepository->getLastDocumentByPartAndType($part, 'secondaryFile');
        
        return $this->render('part/show.html.twig', [
            'part' => $part,
            'primaryFile'	  => $primaryFile,
            'secondaryFile'	  => $secondaryFile
        ]);
    }

    #[Route('/{id}/edit', name: 'app_part_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Part $part, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(PartEditType::class, $part);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $part->setLastUser($this->getUser()->getUserIdentifier());
            // Storing uploaded picture
            if ($picture = $form['picture']->getData()) {
                $this->setPicture($part, $picture);
            }

            // Files processing
            $primaryFile = $form->get('primaryFile')->getData();
            $secondaryFile = $form->get('secondaryFile')->getData();
            $document = $form->get('document')->getData();
            if ($this->processFiles($part, $primaryFile, $secondaryFile, $document, $em)) {
                $this->repository->add($part);
                return $this->redirectToRoute('app_part_show', array('id' => $part->getId()), Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('part/edit.html.twig', [
            'part' => $part,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_part_delete', methods: ['POST'])]
    public function delete(Request $request, Part $part): Response
    {
        if ($this->isCsrfTokenValid('delete'.$part->getId(), $request->request->get('_token'))) {
            $this->repository->remove($part);
        }

        return $this->redirectToRoute('app_homepage', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/prevPart/{id}', name: 'app_part_prev', methods: ['GET'])]
    public function getPrevPartAction(Part $part): Response
    {
    	$id = $part->getID();
        $family = $part->getFamily();
    	$type = $part->getType();
    	
        $next_part = $this->repository->getPrevPartWithFamilyAndType($id, $family, $type);
    	if ($next_part == null) {
    		$next_part = $this->repository->getPrevPartWithFamilyAndType(99999, $family, $type);
    	}
    	
    	return $this->redirectToRoute('app_part_show', array('id' => $next_part->getId()));
    }

    #[Route('/nextPart/{id}', name: 'app_part_next', methods: ['GET'])]
    public function getNextPartAction(Part $part): Response
    {
    	$id = $part->getID();
        $family = $part->getFamily();
    	$type = $part->getType();
    	
        $next_part = $this->repository->getNextPartWithFamilyAndType($id, $family, $type);
    	if ($next_part == null) {
    		$next_part = $this->repository->getNextPartWithFamilyAndType(1, $family, $type);
    	}
    	
    	return $this->redirectToRoute('app_part_show', array('id' => $next_part->getId()));
    }

    #[Route('/upgradeMajorRev/{id}', name: 'app_part_upgradeMajor', methods: ['GET'])]
    public function upgradeMajorRevAction(Part $part, EntityManagerInterface $em) {
    	$majorRev = $part->getMajorRev();
    	$majorRev++;
    	$part->setMajorRev($majorRev);
    	$part->setMinorRev('01');
    	
    	$em->flush();
    	
    	// On définit un message flash
    	$this->addFlash('success', "The major revision has been updated. New revision: <strong>".$part->getMajorRev() . $part->getMinorRev() ."</strong>");
    	
    	return $this->redirectToRoute('app_part_show', array('id' => $part->getId())); 
    }

    #[Route('/upgradeMinorRev/{id}', name: 'app_part_upgradeMinor', methods: ['GET'])]
    public function upgradeMinorRevAction(Part $part, EntityManagerInterface $em) {
    	$minorRev = $part->getMinorRev();
    	$minorRev++;
    	$part->setMinorRev(sprintf("%02d", $minorRev));
    	
    	$em->flush();
    	
    	// On définit un message flash
    	$this->addFlash('success', "The minor revision has been updated. New revision: <strong>".$part->getMajorRev() . $part->getMinorRev() ."</strong>");
    	
    	return $this->redirectToRoute('app_part_show', array('id' => $part->getId())); 
    }

    #[Route('/toogleObsolete/{id}', name: 'app_part_toogleObsolete', methods: ['GET'])]
    public function toogleObsoleteAction(Part $part, EntityManagerInterface $em) {    	 
    	$obsolete = $part->getIsObsolete();
    	$obsolete = !$obsolete;
    	$part->setIsObsolete($obsolete);
    	$em->flush();
    	 
    	// On définit un message flash
    	$this->addFlash('success', "The part has been updated");
    
    	return $this->redirectToRoute('app_part_show', array('id' => $part->getId())); 
    }

    // Checking the existence of a specified reference (+ family)
    private function checkUnicity($family, $type, $number) {
        $part = $this->repository->findOneBy(
        		array(
        			'family' => $family,
        			'type' => $type,
        			'number' => $number));
        
        if ($part != null) {
        	return true;
        }
        else {
        	return false;
        }
    }

    private function setPicture(Part $part, $picture) {
        $filename = bin2hex(random_bytes(6)).'.'.$picture->guessExtension();
        try {
            $picture->move($this->getParameter('picture_root_dir'), $filename);
            $this->imageOptimizer->resize($this->getParameter('picture_root_dir').'/'.$filename);
        } catch (FileException $e) {
            // unable to upload the picture, give up
            $this->addFlash('danger', "Unable to upload the picture.");
        }
        $part->setpictureFilename($filename);
    }

    // Return a list of the last n documents for a specified part
    // Passing part_id as an argument avoid the use of param converter and a database request
    #[Route('/listDocuments/{part_id}', name: 'documents_list')]
    public function getListDocumentsAction(Int $part_id): Response
    {
        // For document, we retrieve a list of the last documents (to be moved in a dedicated controller to load modal with Stimulus)
        $list_documents = $this->documentRepository->getDocumentsListByPartAndType($part_id, 'document', 6);
            
        return $this->render('document/documents.html.twig', [
            'list_documents' => $list_documents,
            'list_primary' => [],
            'list_secondary' => []
        ]);
    }

    // Return a list of old revisions documents (primary and secondary) for a part
    // Passing part_id as an argument avoid the use of param converter and a database request
    #[Route('/oldRevisions/{part_id}', name: 'old_revisions')]
    public function getListOldRevisionsAction(Int $part_id): Response
    {
        // For document, we retrieve a list of the last documents (to be moved in a dedicated controller to load modal with Stimulus)
        $list_primary = $this->documentRepository->getDocumentsListByPartAndType($part_id, 'primaryFile', 6);
        $list_secondary = $this->documentRepository->getDocumentsListByPartAndType($part_id, 'secondaryFile', 6);
            
        return $this->render('document/documents.html.twig', [
            'list_documents' => [],
            'list_primary' => $list_primary,
            'list_secondary' => $list_secondary
        ]);
    }

    // Generate the next number for a part based on family and type
    // ToDo: replace with Stimulus event...
    #[Route('/nextNumber/ajax', name: 'app_part_newNumber')]
    public function getNextNumberAction(Request $request): Response
    {
        $idFamily = $request->query->get('idFamily');
        $idType = $request->query->get('idType');
        $lastPart = null;
        
        if ($idFamily != null & $idType != null) {
            $lastPart = $this->repository->getLastNumber($idFamily, $idType);

            if ($lastPart != null) {
                $newNumber = $lastPart->getNumber() + 1;
                $newNumberText = sprintf('%05d', $newNumber);
            }
            else {
                $newNumberText = '00001';
            }
        }
        else {
            $newNumberText = 'Please choose family and type';
        }
            
        $response = new Response();
        $response->setContent($newNumberText);
        return $response;
    }

    // Processing submited files
    private function processFiles(Part $part, Document $primaryFile = null, Document $secondaryFile = null, Document $document = null, EntityManagerInterface $em) {
        // Check the submitted documents to see if they match our internal constraints.        
        if ($this->checkName($part, $primaryFile) && $this->checkName($part, $secondaryFile)) {                
            // Checking for duplicates
            if ($this->isUniqueDocument($primaryFile) && $this->isUniqueDocument($secondaryFile) && $this->isUniqueDocument($document)) {
                // Adding (and persisting) the part
                $this->repository->add($part);
                // Linking the processed files to the entity
                if ($primaryFile != null) {
                    $em->persist($primaryFile);
                    $primaryFile->setPart($part);
                    $primaryFile->setType('primaryFile');
                }
                if ($secondaryFile != null) {
                    $em->persist($secondaryFile);
                    $secondaryFile->setPart($part);
                    $secondaryFile->setType('secondaryFile');
                }
                if ($document != null) {
                    $em->persist($document);
                    $document->setPart($part);
                    $document->setType('document');
                }

                return true;
            }
        }
        return false;
    }

    // Checking the document to implement some verification 
    private function checkName(Part $part, Document $document = null) {
        if ($document) {
            $expected_prefix  = $part->getFamily()->getCode().'-'.$part->getType()->getCode().$part->getNumber().'-'.$part->getMajorRev().$part->getMinorRev();
            if ($file = $document->getFile()) {
                $filename = $file->getClientOriginalName();
                // We use strpos to validate the filename prefix.
                // This guarantee the filename and revision index before upload
                if (strpos($filename, $expected_prefix) !== 0) {
                    // Flash d'echec
                    $this->addFlash('danger', "The submited file: <strong>". $filename . "</strong> does not respect the part name or revision (<small class='text-muted'>". $expected_prefix ."-xxx</small>).");
                    return false;
                }
            }
        }
        return true;
    }

    // Checking the document for a possible duplicate
    private function isUniqueDocument(Document $document = null) {
        if ($document) {
            if ($file = $document->getFile()) {
                $filename = $file->getClientOriginalName();

                $match = $this->documentRepository->findOneByFilename($filename);

                if($match != null) {
                    $part = $match->getPart();
                    $match_prefix  = $part->getFamily()->getCode().'-'.$part->getType()->getCode().$part->getNumber();
                    $this->addFlash('danger', "The file <strong>". $match->getFilename()."</strong> linked to part <strong>". $match_prefix ."</strong> already exist.");
                    return false;
                }
                else {
                    // We could want to double check with file prefix but would need to include revision and document type
                    // (there could be a primary and a secondary file with the same prefix/filename but different extension...)
                    // return false;
                }                
            }
        }
        return true;
    }
    
}
