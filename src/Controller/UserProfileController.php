<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\ChangePasswordType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/profile'), IsGranted('ROLE_USER')]
class UserProfileController extends AbstractController
{
    #[Route('/{id}/change-password', name: 'user_change_password', methods: ['GET', 'POST'])]
    public function changePassword(Request $request, User $user, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager): Response
    {
        /** @var User $loggedUser */
        $loggedUser = $this->getUser();

        // If user does not have ROLE_ADMIN, we check that he is editing his own profile.
        if($this->isGranted('ROLE_ADMIN') || $user == $loggedUser ) {
            $form = $this->createForm(ChangePasswordType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user->setPassword($passwordHasher->hashPassword($user, $form->get('newPassword')->getData()));
                $entityManager->flush();

                $this->addFlash('success', 'Password updated successfully');
                return $this->redirectToRoute('app_homepage');
            }

            return $this->render('user/change_password.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        else {
            $this->addFlash('danger', "You are not authorized to modify this user.");
            return $this->redirectToRoute('app_homepage', [], Response::HTTP_SEE_OTHER);
        }
    }

    #[Route('/{id}/edit', name: 'user_profile', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserRepository $userRepository): Response
    {
        /** @var User $loggedUser */
        $loggedUser = $this->getUser();

        // If user does not have ROLE_ADMIN, we check that he is editing his own profile.
        if($this->isGranted('ROLE_ADMIN') || $user == $loggedUser ) {
            $form = $this->createForm(UserType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $userRepository->add($user);
                return $this->redirectToRoute('app_homepage', [], Response::HTTP_SEE_OTHER);
            }

            return $this->renderForm('user/edit.html.twig', [
                'user' => $user,
                'form' => $form,
            ]);
        }
        else {
            $this->addFlash('danger', "You are not authorized to modify this user.");
            return $this->redirectToRoute('app_homepage', [], Response::HTTP_SEE_OTHER);
        }
    }
}
