<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\PartRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function index(PartRepository $repository): Response
    {
        $list_parts = $repository->getPartList();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'parts' => $list_parts,
        ]);
    }

    #[Route('/allParts', name: 'app_allParts')]
    public function allParts(PartRepository $repository): Response
    {
        $list_parts = $repository->getPartListWithObsolete();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'parts' => $list_parts,
        ]);
    }
    
}
