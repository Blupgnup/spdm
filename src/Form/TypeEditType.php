<?php

namespace App\Form;

use App\Entity\Type;
use App\Form\TypeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeEditType extends TypeType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // We call for the parent form and we will then update the needed fiels
        parent::buildForm($builder, $options);
        $builder
            ->add('code', null, ['disabled' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Type::class,
            'attr' => [
                'novalidate' => 'novalidate', // We disable client side validation as we use symfony/validator
            ],
        ]);
    }
}
