<?php

namespace App\Form;

use App\Entity\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', null, [
                'help' => 'It is recommended to choose a 3 (max 5) digit alphabetic code',
            ])
            ->add('signification')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Type::class,
            'attr' => [
                'novalidate' => 'novalidate', // We disable client side validation as we use symfony/validator
            ],
        ]);
    }
}
