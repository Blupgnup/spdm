<?php

namespace App\Form;

use App\Entity\Part;
use App\Entity\Type;
use App\Entity\Project;
use App\Entity\Supplier;
use App\Entity\Material;
use App\Form\PartType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartEditType extends PartType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // We call for the parent form and we will then update the needed fiels
        parent::buildForm($builder, $options);
        $builder
            ->add('family', EntityType::class, ['class' => Project::class, 'choice_label' => 'choiceLabel','multiple' => false,'disabled' => true])
            ->add('type', EntityType::class, ['class' => Type::class,'choice_label' => 'code','disabled' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Part::class,
            'attr' => [
                'novalidate' => 'novalidate', // We disable client side validation as we use symfony/validator
            ],
        ]);
    }
}
