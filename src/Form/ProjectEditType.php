<?php

namespace App\Form;

use App\Entity\Project;
use App\Form\ProjectType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectEditType extends ProjectType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // We call for the parent form and we will then update the needed fiels
        parent::buildForm($builder, $options);
        $builder
            ->add('code', null, ['disabled' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'attr' => [
                'novalidate' => 'novalidate', // We disable client side validation as we use symfony/validator
            ],
        ]);
    }
}
