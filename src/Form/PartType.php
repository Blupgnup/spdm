<?php

namespace App\Form;

use App\Entity\Part;
use App\Entity\Type;
use App\Entity\Project;
use App\Entity\Supplier;
use App\Entity\Material;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Valid;

class PartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('family', EntityType::class, [
                // looks for choices from this entity
                'class' => Project::class,
            
                // uses the Project getChoiceLabel method to display label for selection
                'choice_label' => 'choiceLabel',
                'placeholder' => 'Choose a family',
                // used to render a select box, check boxes or radios
                'multiple' => false,
                // 'expanded' => true,
            ])
            ->add('type', EntityType::class, [
                // looks for choices from this entity
                'class' => Type::class,
                'placeholder' => 'Choose a type',
                // uses the Type.code property as the visible option string
                'choice_label' => 'code',
            
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('number', null, ['attr' => ['readonly' => true]])
            ->add('designation')
            ->add('majorRev', null)
            ->add('minorRev', null)
            ->add('isCAD')
            ->add('supplierRef')
            ->add('commentary')
            ->add('picture', FileType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new Image(['maxSize' => '1024k'])
                ],
            ])
            ->add('primaryFile', DocumentType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => new Valid()])
            ->add('secondaryFile', DocumentType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => new Valid()])    
            ->add('document', DocumentType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => new Valid()])
            ->add('projects', EntityType::class, [
                // looks for choices from this entity
                'class' => Project::class,
            
                // uses the Project.code property as the visible option string
                'choice_label' => 'code',
            
                // used to render a select box, check boxes or radios
                'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('suppliers', EntityType::class, [
                // looks for choices from this entity
                'class' => Supplier::class,
            
                // uses the supplier.code property as the visible option string
                'choice_label' => 'name',
            
                // used to render a select box, check boxes or radios
                'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('materials', EntityType::class, [
                // looks for choices from this entity
                'class' => Material::class,
            
                // uses the Material.code property as the visible option string
                'choice_label' => 'name',
            
                // used to render a select box, check boxes or radios
                'multiple' => true,
                // 'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Part::class,
            'attr' => [
                'novalidate' => 'novalidate', // We disable client side validation as we use symfony/validator
            ],
        ]);
    }
}
