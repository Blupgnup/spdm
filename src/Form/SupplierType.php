<?php

namespace App\Form;

use App\Entity\Supplier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('field')
            ->add('address')
            ->add('contactName')
            ->add('contactFunction')
            ->add('email')
            ->add('phoneStd')
            ->add('phoneDir')
            ->add('comments')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Supplier::class,
            'attr' => [
                'novalidate' => 'novalidate', // We disable client side validation as we use symfony/validator
            ],
        ]);
    }
}
