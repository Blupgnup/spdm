<?php

namespace App\Repository;

use App\Entity\Part;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Part|null find($id, $lockMode = null, $lockVersion = null)
 * @method Part|null findOneBy(array $criteria, array $orderBy = null)
 * @method Part[]    findAll()
 * @method Part[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Part::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Part $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Part $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getPartList()
	{
		// QueryBuilder creation
		$qb = $this->createQueryBuilder('p');
		$qb = $this->joinPartWithFamiliesAndTypes($qb);
		
		// Joining suppliers and material to avoid multiple requests
		$qb = $this->joinPartWithAttributes($qb);
		
		// Sort entities
		$qb->where('p.isObsolete = :ob')
     	   ->setParameter('ob', false)
		   ->orderBy('f.code')
		   ->addOrderBy('t.code')
		   ->addOrderBy('p.number');
		
		// Query execution
		return $qb->getQuery()
				  ->getResult();
	}

	public function getPartListWithObsolete()
	{
		// QueryBuilder creation
		$qb = $this->createQueryBuilder('p');
		$qb = $this->joinPartWithFamiliesAndTypes($qb);
		
		// Joining suppliers and material to avoid multiple requests
		$qb = $this->joinPartWithAttributes($qb);
		
		// Sort entities
		$qb->orderBy('f.code')
		   ->addOrderBy('t.code')
		   ->addOrderBy('p.number');
		
		// Query execution
		return $qb->getQuery()
				  ->getResult();
	}

	public function getPartWithDetails($id)
	{
		// QueryBuilder creation
		$qb = $this->createQueryBuilder('p');
		$qb = $this->joinPartWithFamiliesAndTypes($qb);

		// Joining suppliers and material to avoid multiple requests
		$qb = $this->joinPartWithAttributes($qb);

		$qb->where('p.id = :id')
     	   ->setParameter('id', $id);

		// Query execution
		return $qb->getQuery()
				  ->getOneOrNullResult();
		
	}


    public function joinPartWithFamiliesAndTypes($qb)
	{
		// Joining with families and types
        // (we can use join as a part without type or family should not be possible)
		$qb->join('p.family', 'f')
		   ->join('p.type', 't')

           ->addSelect('f', 't');
	
		return $qb;
	}

    public function joinPartWithAttributes($qb)
	{
		// Joining with families and types
        // We need to use leftJoin to make sure that a part without a supplier, 
        // material or project will also be returned.
		$qb->leftJoin('p.suppliers', 'sp')
           ->leftJoin('p.materials', 'ma')
           ->leftJoin('p.projects', 'pr')

		   ->addSelect('sp', 'ma', 'pr');
	
		return $qb;
	}

	public function joinPartWithFiles($qb)
	{
		// Joining with families and types
        // We need to use leftJoin to make sure that a part without a supplier, 
        // material or project will also be returned.
		$qb->leftJoin('p.suppliers', 'sp')
           ->leftJoin('p.materials', 'ma')
           ->leftJoin('p.projects', 'pr')

		   ->addSelect('sp', 'ma', 'pr');
	
		return $qb;
	}

    public function getPrevPartWithFamilyAndType($id, $family, $type)
	{
		$qb = $this->createQueryBuilder('p')
					->leftJoin('p.family', 'f')
					->leftJoin('p.type', 't')
					->where('f = :family')
					->andWhere('t = :type')
					->setParameter('family',  $family)
					->setParameter('type', $type)
					->orderBy('p.id', 'DESC')
					->andwhere('p.id < :id')
					->setParameter('id', $id);
	
		$qb->setMaxResults( 1 );
	
		return $qb->getQuery()
				  ->getOneOrNullResult();
	}

    public function getNextPartWithFamilyAndType($id, $family, $type)
	{
		$qb = $this->createQueryBuilder('p')
					->leftJoin('p.family', 'f')
					->leftJoin('p.type', 't')
					->where('f = :family')
					->andWhere('t = :type')
					->setParameter('family',  $family)
					->setParameter('type', $type)
					->orderBy('p.id')
					->andwhere('p.id > :id')
					->setParameter('id', $id);
	
		$qb->setMaxResults( 1 );
	
		return $qb->getQuery()
				  ->getOneOrNullResult();
	}

    public function getLastNumber($familyId, $typeId)
	{
		$qb = $this->createQueryBuilder('p')
					->leftJoin('p.family', 'f')
					->leftJoin('p.type', 't')
					->where('f.id = :familyId')
					->andWhere('t.id = :typeId')
					->setParameter('familyId',  $familyId)
					->setParameter('typeId', $typeId)
					->orderBy('p.number', 'DESC');
	
		$qb->setMaxResults( 1 );
	
		return $qb->getQuery()
				  ->getOneOrNullResult();
	}

    // /**
    //  * @return Part[] Returns an array of Part objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Part
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
