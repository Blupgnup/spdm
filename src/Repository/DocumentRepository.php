<?php

namespace App\Repository;

use App\Entity\Document;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Document|null find($id, $lockMode = null, $lockVersion = null)
 * @method Document|null findOneBy(array $criteria, array $orderBy = null)
 * @method Document[]    findAll()
 * @method Document[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Document::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Document $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Document $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getLastDocumentByPartAndType($Part, $type): ?Document
	{
		$qb = $this->createQueryBuilder('d')
				   ->where('d.Part = :Part')
				   ->andWhere('d.type = :type')
				   ->setParameter('Part', $Part)
				   ->setParameter('type', $type)
				   ->orderBy('d.title', 'DESC');	
		$qb->setMaxResults( 1 );
	
		return $qb->getQuery()
				  ->getOneOrNullResult();
	}

    /**
     * @return Document[] Returns an array of Document objects
     */
    public function getDocumentsListByPartAndType($Part, $type, int $nb = 1)
	{
		$qb = $this->createQueryBuilder('d')
					->where('d.Part = :Part')
					->andWhere('d.type = :type')
					->setParameter('Part', $Part)
					->setParameter('type', $type)
					->orderBy('d.title', 'DESC');
		$qb->setMaxResults( $nb );
	
		return $qb->getQuery()
				  ->getResult();
	}
}
